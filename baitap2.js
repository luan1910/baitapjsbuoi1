/**
 * Input:
 * number1: 10
 * number2: 20
 * number3: 30
 * number4: 40
 * number5: 50
 * 
 * 
 * Todo:
 * S1: tạo 5 biến lưu trong input: number1,number2,number3,number4,number5
 * S2: tạo 1 biến lưu trong output
 * 
 * S3: Tính giá trị trung bình ta sử dụng công thức: (number1,number2,number3,number4,number5 / 5)
 * 
 * Output:
 * Result: .....Ví Dụ: (10, 20, 30, 40, 50 / 5)
 *              Kết quả: ( 2, 4, 6, 8, 10)
 */           