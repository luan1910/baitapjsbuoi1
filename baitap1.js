/**
 * Input:
 * Lương 1 ngày: 100.000
 * Số ngày làm: cho người dùng nhập
 * 
 * 
 * 
 * Todo:
 * S1: tạo 2 biến lưu trong input (Lương 1 ngày: 100.000
 *                                 Số ngày làm: cho người dùng nhập)
 * S2: tạo 1 biến lưu trong output
 * 
 * S3: sử dụng công thức tính lương: Lương 1 ngày * Số ngày làm
 * 
 * Output:
 * Result: .... Ví Dụ: Lương 1 ngày : 100.000
 *                     Số ngày làm:   3
 *                     Result:        300.000
 */